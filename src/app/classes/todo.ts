export class ToDo {
  id: number;
  title: string;
  description: string;
  isComplete: boolean;
}
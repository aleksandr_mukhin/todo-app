import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { ToDo } from '../classes/todo';
import { Observable } from "rxjs/Observable";
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ConfigService } from './config.service';


@Injectable()
export class ToDoService {

_baseUrl: string = '';
 
    constructor(private http: Http,
        private configService: ConfigService) {
        this._baseUrl = configService.getApiURI();
    }
      
    getToDos(): Observable<ToDo[]> {
     return this.http.get(this._baseUrl + 'todos/')
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    create(todo: ToDo): Observable<ToDo> {
 
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        console.log(JSON.stringify(todo));
        return this.http.post(this._baseUrl + 'todos/', JSON.stringify(todo), {
            headers: headers
        })
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    edit(todo: ToDo): Observable<ToDo> {
 
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
 
        return this.http.put(this._baseUrl + 'todos/', JSON.stringify(todo), {
            headers: headers
        })
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    delete(todo: ToDo): Observable<void> {
 
        return this.http.delete(this._baseUrl + 'todos/' + todo.id)
            .map((res: Response) => {
                return;
            })
            .catch(this.handleError);
    }
    
    private handleError(error: any) {
        var applicationError = error.headers.get('Application-Error');
        var serverError = error.json();
        var modelStateErrors: string = '';
 
        if (!serverError.type) {
            console.log(serverError);
            for (var key in serverError) {
                if (serverError[key])
                    modelStateErrors += serverError[key] + '\n';
            }
        }
 
        modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
 
        return Observable.throw(applicationError || modelStateErrors || 'Server error');
    }
}
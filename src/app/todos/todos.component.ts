import { Component, OnInit } from '@angular/core';

import { ToDo } from '../classes/todo';
import { ToDoService } from '../services/todo.service';
import { ConfigService } from '../services/config.service';

@Component({
  selector: 'todos-app',
  templateUrl: './todos.component.html',
  styleUrls: [ './todos.component.css' ] 
})

export class ToDosComponent implements OnInit {

  title = 'To Do';
  today = Date.now();
  selectedToDo: ToDo;
  todos: ToDo[];

  constructor(private toDoService: ToDoService) { }

  getToDos(): void {
    this.toDoService.getToDos()
            .subscribe((res: ToDo[]) => {
                this.todos = res;
            },
            error => {
                
            });
  }

    createToDo(title: string, description: string) {
        var todo = new ToDo();
        todo.title = title;
        todo.description = description;
        this.toDoService.create(todo);
        this.getToDos();
    }

    completeToDo(todo: ToDo) {
        todo.isComplete = true;
        this.toDoService.edit(todo);
    }

    editToDo(todo: ToDo) {
        this.toDoService.edit(todo);
    }

    deleteToDo(todo: ToDo){
      this.toDoService.delete(todo);
    }

  ngOnInit(): void {
    this.getToDos();
  }

    onSelect(todo: ToDo): void {
    this.selectedToDo = todo;
  }
}
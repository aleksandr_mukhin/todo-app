import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppComponent }  from './app.component';
import { ToDosComponent }     from './todos/todos.component';
//import { ToDoDetailComponent } from './todos/todo-detail.component';

import { ToDoService } from './services/todo.service';
import { ConfigService } from "./services/config.service";

@NgModule({
  imports:      [ BrowserModule, FormsModule, HttpModule ],
  declarations: [ AppComponent, ToDosComponent ],
  bootstrap:    [ AppComponent ],
  providers:    [ ToDoService, ConfigService ]
})
export class AppModule { }
